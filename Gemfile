# frozen_string_literal: true

source 'https://rubygems.org'
ruby '2.4.2'

# Core
gem 'rails', '~> 5.1.4'
gem 'pg', '~> 0.18'

# Orchestration
gem 'puma', '~> 3.10'
gem 'foreman', '~> 0.84'
gem 'dotenv-rails', '~> 2.1'

# Asset processing
gem 'webpacker', '~> 3.2'
gem 'sassc-rails', '~> 1.2'
gem 'uglifier', '>= 1.3'

# Views
gem 'active_model_serializers', '~> 0.10'
gem 'slim-rails', '~> 3.1'

# Background jobs
gem 'sidekiq'

# Logging
gem 'lograge', '~> 0.6'

group :development, :test do
  # Debugging
  gem 'byebug', platform: :mri

  # Testing
  gem 'rspec-rails', '~> 3.7'
  gem 'factory_girl_rails'
  gem 'faker'

  # Code Quality
  gem 'rubocop', require: false
end

group :development do
  # Debugging
  gem 'web-console', '~> 3.3'
  gem 'listen', '~> 3.0'

  # Preloading
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0'

  # Testing
  gem 'letter_opener'
  gem 'meta_request'
  gem 'overcommit', require: false
end

group :test do
  # Testing
  gem 'mocha'
  gem 'webmock'
end
