# frozen_string_literal: true

require 'rails_helper'

RSpec.describe "Bookings", type: :request do
  describe "GET /api/bookings/:id" do
    describe 'for a valid number' do
      let(:do_request) { get api_booking_path('TXG790195100') }

      it "creates record for untracked numbers" do
        expect do
          do_request
          expect(response).to have_http_status(:ok)
        end.to(change { Booking.count })
      end

      it 'returns existing record' do
        do_request
        expect do
          do_request
          expect(response).to have_http_status(:ok)
        end.to_not(change { Booking.count })
      end
    end

    describe 'for an invalid number' do
      let(:do_request) { get api_booking_path('dummy') }

      it 'returns 404 error' do
        do_request
        expect(response).to have_http_status(:not_found)
        expect(json_response['error']).to eq('Not Found')
      end

      it 'does not create a record' do
        expect do
          do_request
        end.to_not(change { Booking.count })
      end
    end
  end
end
