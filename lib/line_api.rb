# frozen_string_literal: true

require 'line_api/apis/pil'

class LineAPI
  class NotImplemented < StandardError; end
  class UnsupportedAPI < StandardError; end

  HANDLERS = {
    /.*/ => LineAPI::APIS::PIL
  }.freeze

  attr_reader :number

  def self.retrieve(number)
    HANDLERS.each do |(matcher, api)|
      return api.new(number).retrieve if matcher.match?(number)
    end
    raise UnsupportedAPI
  end

  def initialize(number)
    @number = number
  end

  def retrieve
    raise NotImplemented
  end
end
