# frozen_string_literal: true

require 'line_api'

class LineAPI
  module APIS
    class PIL < ::LineAPI
      def retrieve
        mock_file = Rails.root.join("lib/mock/#{number.to_s.downcase}.json")
        return unless File.exist?(mock_file)
        parse_json File.read(mock_file)
      end

      private

      def parse_json(json_string)
        json         = JSON.parse(json_string)
        first_update = json.fetch('updates', [{}]).first
        {
          booking_number:        json['booking_number'],
          steamship_line:        json['steamship_line'],
          origin:                json['origin'],
          destination:           json['destination'],
          vessel:                first_update['vessel'],
          voyage:                first_update['voyage'],
          vessel_eta:            first_update['vessel_eta'],
          containers_attributes: parse_containers(json)
        }
      end

      def parse_containers(json)
        json['containers'].map do |container|
          {
            number:                       container['number'],
            size:                         container['size'],
            type:                         container['type'],
            location:                     container['location'],
            last_status:                  container['last_status'],
            last_status_at:               container['last_status_at'],
            container_updates_attributes: parse_container_updates(json, container['number'])
          }
        end
      end

      def parse_container_updates(json, container_number)
        json['updates'].select { |update| update['container_number'] == container_number }.map do |update|
          {
            arrival:     update['arrival'],
            delivery_on: update['delivery_on']
          }
        end
      end
    end
  end
end
