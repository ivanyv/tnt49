# frozen_string_literal: true

class CreateBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :bookings do |t|
      t.citext :booking_number
      t.string :steamship_line
      t.string :origin
      t.string :destination
      t.string :vessel
      t.string :voyage
      t.date :vessel_eta

      t.timestamps
    end

    add_index :bookings, :booking_number, unique: true
  end
end
