# frozen_string_literal: true

class CreateContainerUpdates < ActiveRecord::Migration[5.1]
  def change
    create_table :container_updates do |t|
      t.references :container, index: true
      t.string :arrival
      t.string :delivery_on

      t.timestamps
    end
  end
end
