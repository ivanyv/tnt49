# frozen_string_literal: true

class CreateContainers < ActiveRecord::Migration[5.1]
  def change
    create_table :containers do |t|
      t.references :booking, index: true
      t.string :number
      t.string :size
      t.string :type
      t.string :location
      t.string :last_status
      t.datetime :last_status_at

      t.timestamps
    end
  end
end
