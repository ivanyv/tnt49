# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    resources :bookings, only: %i[index show]
  end

  root to: 'application#index'
  get '*react' => 'application#index'
end
