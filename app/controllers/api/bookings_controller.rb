# frozen_string_literal: true

class Api::BookingsController < ApiController
  def index
    respond_with Booking.by_numbers(params[:booking_numbers]).includes(containers: :container_updates), include: '**'
  end

  def show
    respond_with Booking::Tracker.find(params[:id]), include: '**'
  end
end
