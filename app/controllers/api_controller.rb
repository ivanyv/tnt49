# frozen_string_literal: true

class ApiController < ApplicationController
  private

  def respond_with(response, options = {})
    if response.is_a?(NotFound)
      render json: response, status: :not_found
    else
      render options.merge(json: response)
    end
  end
end
