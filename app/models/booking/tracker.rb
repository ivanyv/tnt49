# frozen_string_literal: true

require 'line_api'

class Booking
  class Tracker
    attr_reader :number, :result

    def self.find(number)
      existing = Booking.find_by(booking_number: number)
      return existing if existing
      tracker = new(number)
      tracker.retrieve_and_create!
      tracker.result
    end

    def initialize(number)
      @number = number
    end

    def retrieve_and_create!
      @result =
        if (response = LineAPI.retrieve(number))
          create_booking response
        else
          Booking::NotFound.new message: 'Booking number not found'
        end
    end

    private

    def create_booking(attributes)
      Booking.create! attributes
    end
  end
end
