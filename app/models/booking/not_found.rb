# frozen_string_literal: true

class Booking
  class NotFound < ::NotFound
    attr_accessor :number
  end
end
