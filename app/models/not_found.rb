# frozen_string_literal: true

class NotFound
  include ActiveModel::Model
  include ActiveModel::Serialization

  attr_accessor :message

  def error
    'Not Found'
  end
end
