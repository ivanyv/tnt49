# frozen_string_literal: true

class Booking < ApplicationRecord
  has_many :containers, dependent: :destroy

  accepts_nested_attributes_for :containers

  scope :by_numbers, ->(numbers) do
    if numbers.is_a?(Array)
      where(booking_number: numbers.map(&:presence).compact)
    else
      none
    end
  end
end
