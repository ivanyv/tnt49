# frozen_string_literal: true

class ContainerUpdate < ApplicationRecord
  belongs_to :container
end
