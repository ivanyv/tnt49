# frozen_string_literal: true

class Container < ApplicationRecord
  self.inheritance_column = '_none'

  belongs_to :booking
  has_many :container_updates, dependent: :delete_all

  accepts_nested_attributes_for :container_updates
end
