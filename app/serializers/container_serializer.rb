# frozen_string_literal: true

class ContainerSerializer < ActiveModel::Serializer
  attributes :id, :number, :size, :type, :location, :last_status, :last_status_at

  has_many :container_updates
end
