# frozen_string_literal: true

class BookingSerializer < ActiveModel::Serializer
  attributes :id, :booking_number, :steamship_line, :origin, :destination, :vessel, :voyage, :vessel_eta

  has_many :containers
end
