# frozen_string_literal: true

class ContainerUpdateSerializer < ActiveModel::Serializer
  attributes :id, :arrival, :delivery_on
end
