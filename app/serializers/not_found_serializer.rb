# frozen_string_literal: true

class NotFoundSerializer < ActiveModel::Serializer
  attributes :error, :message
end
