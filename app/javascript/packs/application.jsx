import React from 'react';
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect, Provider } from 'react-redux';

import store, * as actions from '../store';

import API from 'lib/api';
const api = new API();

const Bookings = {
  Index: require("components/bookings").default,
  Show:  require("components/bookings/show").default
};

const Errors = {
  NotFound: require("components/errors/not-found").default
};

class Application extends React.Component {
  componentWillMount() {
    api.get('bookings', { params: { booking_numbers: this.props.savedBookingNumbers }})
      .then(response => {
        let bookings = {};
        for (let booking of response.data) bookings[booking.id] = booking;
        store.dispatch(actions.loadSavedBookings(bookings));
      });
  }

  render() {
    return (
      <div className="container px-0">
        <h1 className="company my-2 my-sm-4 text-center text-sm-left">
          <span className="fa fa-ship mr-2"/>
          TracknTrace 49
        </h1>
        <Router>
          <Switch>
            <Route exact path="/" component={Bookings.Index}/>
            <Route exact path="/bookings" component={Bookings.Index}/>
            <Route exact path="/bookings/:bookingNumber" component={Bookings.Show}/>
            <Route path="/" component={Errors.NotFound}/>
          </Switch>
        </Router>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    savedBookings:       state.savedBookings,
    savedBookingNumbers: state.savedBookingNumbers
  };
};

const ApplicationContainer = connect(mapStateToProps)(Application);

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Provider store={store}>
      <ApplicationContainer/>
    </Provider>,
    document.body.appendChild(document.createElement('div')),
  )
});
