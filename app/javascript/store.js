import { createStore } from 'redux';

const SAVE_BOOKING        = 'SAVE_BOOKING';
const REMOVE_BOOKING      = 'REMOVE_BOOKING';
const LOAD_SAVED_BOOKINGS = 'LOAD_SAVED_BOOKINGS';

export function saveBooking(booking) {
  return {
    type: SAVE_BOOKING,
    booking
  }
}

export function removeBooking(booking) {
  return {
    type: REMOVE_BOOKING,
    booking
  }
}

export function loadSavedBookings(bookings) {
  return {
    type: LOAD_SAVED_BOOKINGS,
    bookings
  }
}

if (!localStorage.getItem('savedBookingNumbers')) localStorage.setItem('savedBookingNumbers', '');
let savedBookingNumbers = localStorage.getItem('savedBookingNumbers').split('|');

const initialState = {
  savedBookingNumbers: savedBookingNumbers,
  savedBookings: {}
};

function storeBookingNumber(number) {
  let numbers = localStorage.getItem('savedBookingNumbers').split('|');
  numbers.push(number);
  localStorage.setItem('savedBookingNumbers', numbers.join('|'));
}

function removeBookingNumber(number) {
  let numbers = localStorage.getItem('savedBookingNumbers').split('|').filter(saved => saved !== number);
  localStorage.setItem('savedBookingNumbers', numbers.join('|'));
}

const reducer = function(state = initialState, action) {
  let savedBookings = { ...state.savedBookings };
  switch (action.type) {
    case SAVE_BOOKING:
      savedBookings[action.booking.id] = action.booking;
      storeBookingNumber(action.booking.booking_number);
      return { ...state, savedBookings: savedBookings };
    case REMOVE_BOOKING:
      delete savedBookings[action.booking.id];
      removeBookingNumber(action.booking.booking_number);
      return { ...state, savedBookings: savedBookings };
    case LOAD_SAVED_BOOKINGS:
      return { ...state, savedBookings: action.bookings };
  }

  return state;
};

export default createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
