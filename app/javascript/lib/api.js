import axios from 'axios';

export default class Api {
  constructor() {
    this.axios = axios.create({
      baseURL: '/api/',
      headers: {
        'Accept': 'application/json'
      }
    })
  }

  get(path, options = {}) {
    return this.perform('get', path, options)
  }

  perform(method, path, options = {}) {
    return this.axios.get(path, { ...options,
      method: method
    });
  }
}
