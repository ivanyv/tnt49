import React from 'react';
import * as moment from 'moment';
import { Link } from 'react-router-dom';
import NotFound from "../errors/not-found";
import ContainerRow from "../containers/row";
import ContainerUpdates from "../containers/updates";

import { connect } from 'react-redux';
import store, * as actions from '../../store';

import API from 'lib/api';
const api = new API();

class Show extends React.Component {
  constructor(props) {
    super(props);

    this.saveBooking = this.saveBooking.bind(this);
    this.removeBooking = this.removeBooking.bind(this);

    let booking = props.location.state && props.location.state.booking;

    this.state = {
      loading: !booking,
      found: null,
      saved: booking && this.props.savedBookings[booking.id],
      bookingNumber: booking ? booking.booking_number : this.props.match.params.bookingNumber,
      booking: booking,
      expandedContainers: []
    };
  }

  componentWillMount() {
    if (this.state.booking) return;

    api.get(`bookings/${this.state.bookingNumber}`)
      .then(response => {
        this.setState({
          booking: response.data,
          loading: false,
          found: true,
          saved: !!this.props.savedBookings[response.data.id]
        });
      })
      .catch(() => {
        this.setState({ loading: false, found: false });
      });
  }

  toggleContainerUpdates(e, id) {
    e.preventDefault();
    let newValue;
    if (this.state.expandedContainers.indexOf(id) === -1) {
      newValue = this.state.expandedContainers.slice(0);
      newValue.push(id);
    } else {
      newValue = this.state.expandedContainers.filter(inArray => inArray !== id);
    }
    this.setState({ expandedContainers: newValue });
  }

  saveBooking() {
    store.dispatch(actions.saveBooking(this.state.booking));
    this.setState({ saved: true });
  }

  removeBooking() {
    store.dispatch(actions.removeBooking(this.state.booking));
    this.setState({ saved: false });
  }

  render() {
    if (this.state.found === false) return <NotFound/>;

    return (
      <div className="card main">
        <div className="card-header">
          <Link to="/" className="text-secondary">&laquo; Back</Link>
          <h4 className="mb-0">B/L Number: {this.state.bookingNumber}</h4>
        </div>
        <div className="card-body pb-1">
          {this.state.loading &&
          <div className="alert alert-info">Loading...</div>
          }
          {!this.state.loading &&
          <div>
            {this.state.saved &&
            <p>
              <i className="fa fa-check text-success mr-2"/>
              <span className="text-secondary mr-2">You saved this number on your tracking list.</span>
              <button onClick={this.removeBooking} className="btn btn-danger btn-sm mr-2">
                <i className="fa fa-trash mr-2"/>
                Remove
              </button>
            </p>
            }
            {!this.state.saved &&
            <p>
              <button onClick={this.saveBooking} className="btn btn-secondary btn-sm mr-2">
                <i className="fa fa-save mr-2"/>
                Save
              </button>
              <span className="text-secondary">Save this to your list for easy tracking</span>
            </p>
            }

            <ul className="nav nav-tabs mb-3">
              <li className="nav-item">
                <a href="#overview" data-toggle="tab" className="nav-link active">
                  <i className="fa fa-map-o mr-2"/>
                  Overview
                </a>
              </li>
              <li className="nav-item">
                <a href="#containers" data-toggle="tab" className="nav-link">
                  <i className="fa fa-truck mr-2"/>
                  Containers
                </a>
              </li>
            </ul>

            <div className="tab-content">
              <div className="tab-pane fade show active px-3 px-sm-0" id="overview">
                <div className="row">
                  <div className="col-12 col-sm-6 col-md-4 mb-3">
                    <h6 className="mb-0">{this.state.booking.steamship_line}</h6>
                    <span className="small text-secondary">Steamship Line</span>
                  </div>

                  <div className="col-12 col-sm-6 col-md-4 mb-3">
                    <h6 className="mb-0">{this.state.booking.origin}</h6>
                    <span className="small text-secondary">Origin</span>
                  </div>

                  <div className="col-12 col-sm-6 col-md-4 mb-3">
                    <h6 className="mb-0">{this.state.booking.destination}</h6>
                    <span className="small text-secondary">Destination</span>
                  </div>

                  <div className="col-12 col-sm-6 col-md-4 mb-3">
                    <h6 className="mb-0">{this.state.booking.vessel}</h6>
                    <span className="small text-secondary">Vessel</span>
                  </div>

                  <div className="col-12 col-sm-6 col-md-4 mb-3">
                    <h6 className="mb-0">{this.state.booking.voyage}</h6>
                    <span className="small text-secondary">Voyage</span>
                  </div>

                  <div className="col-12 col-sm-6 col-md-4 mb-3">
                    <h6 className="mb-0">{this.state.booking.vessel_eta}</h6>
                    <span className="small text-secondary">ETA</span>
                  </div>
                </div>
              </div>

              <div className="tab-pane fade" id="containers">
                <table className="table table-responsive-sm">
                  <thead className="thead-dark">
                    <tr>
                      <th>Number</th>
                      <th className="d-table-cell d-md-none">Size/Type</th>
                      <th className="d-none d-md-table-cell">Size</th>
                      <th className="d-none d-md-table-cell">Type</th>
                      <th><span className="d-none d-md-inline">Current </span>Location</th>
                      <th><span className="d-none d-md-inline">Last </span>Status</th>
                    </tr>
                  </thead>

                  <tbody>
                    {this.state.booking.containers.map(container =>
                    <ContainerRow key={container.id}>
                      <tr>
                        <td>
                          <a href="#" onClick={(e) => this.toggleContainerUpdates(e, container.id)}>
                            {this.state.expandedContainers.indexOf(container.id) === -1 &&
                            <i className="fa fa-plus mr-2"/>
                            }
                            {this.state.expandedContainers.indexOf(container.id) > -1 &&
                            <i className="fa fa-minus mr-2"/>
                            }
                            {container.number}
                          </a>
                        </td>
                        <td className="d-table-cell d-md-none">{container.size}/{container.type}</td>
                        <td className="d-none d-md-table-cell">{container.size}</td>
                        <td className="d-none d-md-table-cell">{container.type}</td>
                        <td>{container.location}</td>
                        <td>
                          {container.last_status}
                          <div className="small text-secondary">
                            {moment(container.last_status_at).format('YYYY-MM-DD H:mm')}
                          </div>
                        </td>
                      </tr>
                      <tr className="table-secondary"
                          style={{ display: this.state.expandedContainers.indexOf(container.id) === -1 ? 'none' : 'table-row' }}>
                        <td colSpan="5" className="p-0 pl-5 d-none d-md-table-cell"><ContainerUpdates
                          container={container}/></td>
                        <td colSpan="4" className="p-0 d-table-cell d-md-none"><ContainerUpdates container={container}/>
                        </td>
                      </tr>
                    </ContainerRow>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    savedBookings: state.savedBookings
  };
};

export default connect(mapStateToProps)(Show);
