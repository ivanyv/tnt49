import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from "react-redux";
import store, * as actions from '../../store';

class List extends React.Component {
  static removeBooking(booking) {
    store.dispatch(actions.removeBooking(booking));
  }

  render() {
    return (
      <div>
        {this.props.bookings.length === 0 &&
        <div className="alert alert-info mb-0">Saved bookings will appear here.</div>
        }
        {this.props.bookings.length > 0 &&
        <table className="table table-responsive-md mb-0">
          <thead>
          <tr>
            <th>Number</th>
            <th>Line</th>
            <th>Origin</th>
            <th>Destination</th>
            <th>ETA</th>
            <th/>
          </tr>
          </thead>
          <tbody>
            {this.props.bookings.map(booking =>
              <tr key={booking.id}>
                <td><Link to={`bookings/${booking.booking_number}`}>{booking.booking_number}</Link></td>
                <td>{booking.steamship_line}</td>
                <td>{booking.origin}</td>
                <td>{booking.destination}</td>
                <td>{booking.vessel_eta}</td>
                <td className="text-right">
                  <button onClick={() => this.constructor.removeBooking(booking)} className="btn btn-sm btn-danger">
                    <i className="fa fa-trash mr-2"/>
                    Remove
                  </button>
                </td>
              </tr>
            )}
          </tbody>
        </table>
        }
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  let bookings = [];
  for (let id in state.savedBookings) {
    bookings.push(state.savedBookings[id]);
  }

  return {
    bookings: bookings
  };
};

export default connect(mapStateToProps)(List);
