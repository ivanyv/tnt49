import React from 'react';

import { connect } from 'react-redux';
import store, * as actions from '../store';

import API from 'lib/api';
const api = new API();

const Bookings = {
  List: require("components/bookings/list").default,
};

class Index extends React.Component {
  constructor(props) {
    super(props);

    this.trackBooking = this.trackBooking.bind(this);
    this.handleBLNumChange = this.handleBLNumChange.bind(this);

    this.state = {
      error: null,
      loading: false,
      bookingNumber: ''
    }
  }

  handleBLNumChange(e) {
    this.setState({ bookingNumber: e.target.value })
  }

  trackBooking() {
    this.setState({ error: null, loading: true });
    api.get(`bookings/${this.state.bookingNumber}`)
      .then(response => {
        let booking = response.data;
        this.props.history.push(`/bookings/${booking.booking_number}`, { booking: booking });
      })
      .catch(error => {
        if (error.response && error.response.status === 404) {
          this.setState({ loading: false, error: error.response.data['message'] });
        } else {
          this.setState({ loading: false, error: 'There was an unexpected error, please try again later.' });
        }
      });
  }

  isValidBLNum() {
    return this.state.bookingNumber.trim().length > 0;
  }

  render() {
    return (
      <div className="card main">
        <div className="card-header">
          <h4 className="mb-0">Track a Shipment</h4>
          <hr/>
          <div className="form-inline mb-1">
            <div className="form-group mb-0 w-100">
              <label className="mr-2" htmlFor="bl-number">B/L Number:</label>
              <input onChange={this.handleBLNumChange} value={this.state.bookingNumber} id="bl-number" type="text"
                     className="form-control mr-2"/>
              <button onClick={this.trackBooking} className="btn btn-primary mt-2 mt-sm-0"
                      disabled={!this.isValidBLNum()}>Track
              </button>
            </div>
          </div>
          {this.state.error &&
          <div className="text-danger mt-2">{this.state.error}</div>
          }
        </div>
        <div className="card-body">
          <Bookings.List/>
        </div>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    savedBookings: state.savedBookings
  };
};

export default connect(mapStateToProps)(Index);
