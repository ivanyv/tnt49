import React from 'react';

export default class ContainerUpdates extends React.Component {
  render() {
    return (
      <table className="table table-sm table-plain mb-0">
        <thead>
          {this.props.container.container_updates.length > 0 &&
          <tr>
            <th colSpan="2">Updates for container {this.props.container.number}</th>
          </tr>
          }
          {this.props.container.container_updates.length > 0 &&
          <tr>
            <th>Arrival</th>
            <th>Delivery On</th>
          </tr>
          }
          {this.props.container.container_updates.length === 0 &&
          <tr>
            <th>No updates for container {this.props.container.number}</th>
          </tr>
          }
        </thead>
        <tbody>
        {this.props.container.container_updates.map(update =>
        <tr key={update.id}>
          <td>{update.arrival}</td>
          <td>{update.delivery_on}</td>
        </tr>
        )}
        </tbody>
      </table>
    );
  }
}
