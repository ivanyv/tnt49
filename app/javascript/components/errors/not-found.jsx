import React from 'react';

export default class NotFound extends React.Component {
  render() {
    return (
      <div className="card">
        <div className="card-body pa-5 text-center">
          <i className="fa fa-binoculars fa-5x mt-5 mb-3"/>
          <h2>Not Found</h2>
          <p className="text-secondary mb-5">
            The page you were looking for does not exist. We'll track and trace it back if possible.
          </p>
        </div>
      </div>
    )
  }
}
